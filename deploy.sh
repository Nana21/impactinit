#!/bin/sh -x
PROJECT_ENV_REF=$CI_PROJECT_NAME-$CI_ENVIRONMENT_SLUG
DOMAIN_NAME=`echo "$CI_ENVIRONMENT_URL" | awk -F/ '{print $3}'`

sed -i "s/__CI_PROJECT_NAME__/${CI_PROJECT_NAME}/" deployment.yaml
sed -i "s/__CI_ENVIRONMENT_SLUG__/${CI_ENVIRONMENT_SLUG}/" deployment.yaml
sed -i "s#__DOMAIN_NAME__#${DOMAIN_NAME}#" deployment.yaml
sed -i "s/__CI_PROJECT_NAME__/${CI_PROJECT_NAME}/" deployment.yaml
sed -i "s/__PROJECT_ENV_REF__/${PROJECT_ENV_REF}/" deployment.yaml
sed -i "s#__CONTAINER_IMAGE_BUILD__#${CONTAINER_IMAGE_BUILD}#" deployment.yaml

cat deployment.yaml
kubectl version
kubectl create secret docker-registry gitlab-registry \
 --docker-server="$CI_REGISTRY" --docker-username="$CI_REGISTRY_USER" \
 --docker-password="$CI_REGISTRY_PASSWORD" --docker-email="$GITLAB_USER_EMAIL" \
 -o yaml --dry-run | kubectl replace --force -f -
kubectl apply -f deployment.yaml
kubectl rollout status -w deployment $PROJECT_ENV_REF-deployment

# Delete previous commit tag in image registry
apk add curl
curl --header "JOB-TOKEN: $CI_JOB_TOKEN" \
 $CI_API_V4_URL/projects/owned
curl --header "JOB-TOKEN: $CI_JOB_TOKEN" \
 "$CI_API_V4_URL/projects/$CI_PROJECT_ID/registry/repositories"
curl --header "JOB-TOKEN: $CI_JOB_TOKEN" \
 "$CI_API_V4_URL/projects/$CI_PROJECT_ID/registry/repositories/1/tags"
curl --request DELETE --header "JOB-TOKEN: $CI_JOB_TOKEN" \
 $CI_API_V4_URL/projects/$CI_PROJECT_ID/registry/repositories/1/tags/$CONTAINER_IMAGE_PREV
