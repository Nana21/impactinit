from django.db import models


class Prefix(models.Model):
    name = models.CharField(max_length=10)

    def __str__(self):
        return self.name


class PersonType(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Person(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    title = models.CharField(max_length=50)
    bio = models.TextField()
    image_path = models.CharField(max_length=255)
    person_type = models.ForeignKey(PersonType, on_delete=models.CASCADE)
    prefix = models.ForeignKey(Prefix, on_delete=models.CASCADE, null=True,
        blank=True)

    def __str__(self):
        return '{} {} {}'.format(self.prefix.name if self.prefix else '',
            self.first_name, self.last_name)


class Service(models.Model):
    name = models.CharField(max_length=255)
    short = models.CharField(max_length=50)
    icon = models.CharField(max_length=50, null=True, blank=True)
    slug = models.SlugField()
    blurb = models.CharField(max_length=255)
    text = models.TextField()

    def __str__(self):
        return self.name