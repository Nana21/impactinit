from django.contrib import admin

from .models import PersonType, Prefix, Person

admin.site.register(PersonType)
admin.site.register(Prefix)
admin.site.register(Person)