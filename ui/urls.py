from django.urls import path

from .views import index, home, services, about_us, board, team, contact_us

urlpatterns = [
    path('', home, name='home'),
    # path('services/', services, name='services'),
    path('service/<slug:slug>/', services, name='services'),
    path('about/', about_us, name='about'),
    path('about/board/', board, name='board'),
    path('about/team/', team, name='team'),
    path('contact/', contact_us, name='contact'),
]
