from django.shortcuts import render

from people.models import Person, Service


def index(request):
    data = {'services': Service.objects.all().order_by('pk')}
    return render(request, 'index.html', data)

def home(request):
    data = {'services': Service.objects.all().order_by('pk')}
    return render(request, 'index.html', data)

def services(request, slug=None):
    data = {'services': Service.objects.all().order_by('pk'),
        'service': Service.objects.get(slug=slug)}
    return render(request, 'services.html', data)

def about_us(request):
    data = {'services': Service.objects.all().order_by('pk')}
    return render(request, 'about_us.html', data)

def board(request):
    data = {'people': Person.objects.filter(person_type__name='Board Member'),
        'services': Service.objects.all().order_by('pk')}
    return render(request, 'board.html', data)

def team(request):
    data = {'people': Person.objects.filter(person_type__name='Team'),
        'services': Service.objects.all().order_by('pk')}
    return render(request, 'team.html', data)

def contact_us(request):
    data = {'services': Service.objects.all().order_by('pk')}
    return render(request, 'contact_us.html', data)